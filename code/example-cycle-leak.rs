use std::cell::RefCell;
use std::rc::Rc;

#[derive(Debug, Clone)]
pub struct Cyclic {
    ptr: RefCell<Option<Rc<Cyclic>>>,
    message: &'static str
}

impl Cyclic {
    pub fn new(message: &'static str) -> Cyclic {
        Cyclic {
            ptr: RefCell::new(None),
            message
        }
    }
}

impl Drop for Cyclic {
    fn drop(&mut self) {
        println!("Dropping {:#?}", self)
    }
}

fn cycle_leak() {
    let cycle = Rc::new(Cyclic::new("Goodbye!"));
    // `Drop` will be called if and only if this line is removed,
    // but memory will be allocated either way!
    cycle.ptr.replace(Some(cycle.clone()));
}